Janus is a library to to upload screenshots from [motion](https://motion-project.github.io/) and log events written in go.

It expects the following environment variables to exist:

| Variable|Description |Required?|Example|Default value|
|---------|-|----|-|-------|
|`DOCUMENT_ROOT` | Directory where janus watches for screenshots| :heavy_check_mark:| "/usr/src/app" | "/usr/src/app"|
|`S3_BUCKET`| path to amazon s3 bucket to upload screenshots| :heavy_check_mark:|"kitchen-cam"|"kitchen-cam"|
|`LOG_TABLE_NAME`| amazon dynamo db table name to log events to| :heavy_check_mark:| "kitchen-camera-logs"| "kitchen-camera-logs"|
|`AWS_ACCESS_KEY_ID`| your aws access key| :heavy_check_mark:|||
|`AWS_SECRET_ACCESS_KEY`| your aws secret|:heavy_check_mark:|||
|`AWS_REGION`| aws region the above resources are configured in|:heavy_check_mark:|||

## usage

clone the repo

```bash
git clone https://gitlab.com/reedrichards/janus
```

compile the binary

```bash
make build
```

copy the binary to your rasberry pi

```bash
scp ./janus pi@192.168.1.124:/home/pi
```

run the binary with `motion` also running and walk in front of the camera. When `motion` captures a screenshot, an entry will be made into DynamoDB and the screenshot will be uploaded to S3. From there you can add triggers to add additional logic based on that.
