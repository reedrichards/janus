package main

import (
	// "github.com/aws/aws-sdk-go/aws"
	"crypto/md5"
	"encoding/hex"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/fsnotify/fsnotify"
	"github.com/romana/rlog"
)

// since this tests for length of the string value and the empty string "" has length 0
//it means that when the env variable is set to "" you would get the fallback/default
// value rather than the value (which is trivially "").
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

var DocumentRoot = getEnv("DOCUMENT_ROOT", "/usr/src/app")

var Bucket = getEnv("S3_BUCKET", "kitchen-cam")

var watcher *fsnotify.Watcher

type LogItem struct {
	LogID   string
	Content string
}

var sess = session.Must(session.NewSessionWithOptions(session.Options{
	SharedConfigState: session.SharedConfigEnable,
}))

var LogDynamoDB = dynamodb.New(sess)

// main
func watchFiles(docRoot string) {

	// creates a new file watcher
	watcher, _ = fsnotify.NewWatcher()
	defer watcher.Close()

	// starting at the root of the project, walk each file/directory searching for
	// directories
	if err := filepath.Walk(docRoot, watchDir); err != nil {
		rlog.Error(err)
	}

	//
	done := make(chan bool)

	//
	go func() {
		withSlashDocRoot := docRoot + "/"

		for {
			select {
			// watch for events
			case event := <-watcher.Events:
				switch event.Op {
				case fsnotify.Write:
					path := withSlashDocRoot + replaceNth(event.Name, withSlashDocRoot, "", 1)
					rlog.Info(path)
					upload(path)
				}
				rlog.Infof("EVENT! %s\n", event.String())

				// log event to dynamodb
				item := LogItem{
					LogID:   GetMD5Hash(event.String()),
					Content: event.String(),
				}
				av, err := dynamodbattribute.MarshalMap(item)
				if err != nil {
					rlog.Info(err.Error())
				}
				input := &dynamodb.PutItemInput{
					Item:      av,
					TableName: aws.String(getEnv("LOG_TABLE_NAME", "kitchen-camera-logs")),
				}

				_, err = LogDynamoDB.PutItem(input)
				if err != nil {
					rlog.Info(err.Error())
				}

				rlog.Infof("Log DynamoDB added:  %s\n", event.String())

				// watch for errors
			case err := <-watcher.Errors:
				rlog.Info("ERROR", err)
			}
		}
	}()

	<-done
}

func upload(abs_path string) {
	file, err := os.Open(abs_path)
	if err != nil {
		rlog.Info("ERROR", err)
	}
	defer file.Close()
	uploader := s3manager.NewUploader(sess)

	// Upload the file's body to S3 bucket as an object with the key being the
	// same as the filename.
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(Bucket),

		// Can also use the `filepath` standard library package to modify the
		// filename as need for an S3 object key. Such as turning absolute path
		// to a relative path.
		Key: aws.String(filepath.Base(abs_path)),

		// The file to be uploaded. io.ReadSeeker is preferred as the Uploader
		// will be able to optimize memory when uploading large content. io.Reader
		// is supported, but will require buffering of the reader's bytes for
		// each part.
		Body: file,
	})
	if err != nil {
		// Print the error and exit.
		rlog.Errorf("Unable to upload %q to %q, %v", abs_path, Bucket, err)
	}

	rlog.Infof("Successfully uploaded %q to %q\n", abs_path, Bucket)

}

// watchDir gets run as a walk func, searching for directories to add watchers to
func watchDir(path string, fi os.FileInfo, err error) error {

	// since fsnotify can watch all the files in a directory, watchers only need
	// to be added to each nested directory
	if fi.Mode().IsDir() {
		return watcher.Add(path)
	}

	return nil
}

// Replace the nth occurrence of old in s by new.
func replaceNth(s, old, new string, n int) string {
	i := 0
	for m := 1; m <= n; m++ {
		x := strings.Index(s[i:], old)
		if x < 0 {
			break
		}
		i += x
		if m == n {
			return s[:i] + new + s[i+len(old):]
		}
		i += len(old)
	}
	return s
}

// GetMD5Hash get md5 hash of string
func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func main() {
	watchFiles(DocumentRoot)
}
